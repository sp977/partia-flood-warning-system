import datetime
import numpy as np
import operator
from floodsystem.flood import stations_level_over_threshold
from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.station import MonitoringStation
from floodsystem.datafetcher import fetch_measure_levels_no_key_error
from floodsystem.analysis import polyfit, gradient, average_gradient_n_hours_before

def run():
    start_time_of_function = datetime.datetime.now()
    #Tolerances obtained by comparing the list of the stations under warning with the list here https://flood-warning-information.service.gov.uk/warnings
    tolerance_level_extreme = 5
    tolerance_level_high = 1.42
    tolerance_level_low = 1.06
    
    #Create a list of stations to be examined
    stations = build_station_list()
    update_water_levels(stations)

    #Check if the data from the water stations is consistent
    consistent_stations = [station for station in stations if station.typical_range_consistent()]

    #Check if certain stations are already exceeding the tolerances for warnings
    severe_floods = stations_level_over_threshold(consistent_stations,tolerance_level_extreme)
    high_floods = stations_level_over_threshold(consistent_stations, tolerance_level_high)
    low_floods = stations_level_over_threshold(consistent_stations, tolerance_level_low)
    
    """ #Fetching data for the past day
    dt = 1
    n = 1
    stations_with_enough_levels = []
    for i in range(len(consistent_stations)):
        levels_with_nones = fetch_measure_levels_no_key_error(consistent_stations[i].measure_id, datetime.timedelta(days=dt))[1]
        levels = [level for level in levels_with_nones[((n*4)-4):(n*4)] if isinstance(level, float)]
        if len(levels) > 2:
            stations_with_enough_levels.append(consistent_stations[i])
        else:
            pass

    for i in range(len(stations_with_enough_levels)):
        
        #Calculating the average gradient of the relative water level n hours before now by differentiating the polynomial fit
        #To do that each station in the consistent_station list must have at least 2 water levels recorded for the time period specified
        

        levels_with_Nones= fetch_measure_levels_no_key_error(stations_with_enough_levels[i].measure_id, datetime.timedelta(days=dt))[1]
        levels = [level for level in levels_with_Nones[((n*4)-4):(n*4)] if isinstance(level, float)]
        av_grad = average_gradient_n_hours_before( levels, n)
        
        #Check if using the gradient certain stations will get exceed the tolerances for warnings in 3 hours
        #Append the list of severe, high and low risk stations to include those stations 
        if stations_with_enough_levels[i].relative_water_level() + (av_grad*(3*n/24)) > tolerance_level_extreme:
            severe_floods.append(stations_with_enough_levels[i])
        elif stations_with_enough_levels[i].relative_water_level() + (av_grad*(3*n/24)) > tolerance_level_high:
            high_floods.append(stations_with_enough_levels[i])
        elif stations_with_enough_levels[i].relative_water_level() + (av_grad*(3*n/24)) > tolerance_level_low:
            low_floods.append(stations_with_enough_levels[i])
        else:
            pass
        
    #Use sets to get rid of any repeating station names
    list_of_stations_severe_floods = [station[0].name for station in set(severe_floods)]
    list_of_stations_high_floods = [station[0].name for station in set(high_floods)]
    list_of_stations_low_floods = [station[0].name for station in set(low_floods)]
 """
    list_of_stations_severe_floods = [station[0].name for station in set(severe_floods)]
    list_of_stations_high_floods = [station[0].name for station in set(high_floods) - set(severe_floods)]
    list_of_stations_low_floods = [station[0].name for station in set(low_floods) - set(high_floods) - set(severe_floods)]
    #Output the result
    print('''


    The following stations are either already severely flooded already or at a  risk of extreme flood in 3 hours''', list_of_stations_severe_floods)
    print("The length of list of these stations is ", len(list_of_stations_severe_floods))
    print('''
    
    
    The following stations are either already highly flooded or at a risk of being highly flooded in 3 hours''', list_of_stations_high_floods)
    print("The length of list of these stations is ", len(list_of_stations_high_floods))
    print('''
    
    
    The following stations are either already moderately flooded or at a risk of being moderately flooded in 3 hours''', list_of_stations_low_floods)
    print("The length of list of these stations is ", len(list_of_stations_low_floods))
    print("All other stations are at a low risk of flood")
    print("It took {} to execute the function".format(datetime.datetime.now()- start_time_of_function))


if __name__ == "__main__":
    print("*** Task 2G: CUED Part IA Flood Warning System ***")
    run()

