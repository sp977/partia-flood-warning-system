"""Unit test for the analysis module"""


import datetime
from tests_water_stations_for_tests import s1, s2, s3, s4, s5, s_list, dates_list, levels_list
import numpy as np
import matplotlib
from floodsystem.station import MonitoringStation
import pytest
from floodsystem.analysis import polyfit, gradient, average_gradient_n_hours_before

def polyfit_test():
    '''This is a test for the polynomial fitting function. The levels and dates entered for station 2 don't have matching lengths, 
    so a Type Error is meant to come out. Levels for station 4 are not of the float type, so should give a Type Error'''
    with pytest.raises(TypeError):
        polyfit(dates_list[1], dates_list [1], 3)
    with pytest.raises(TypeError):
       polyfit(dates_list[4], dates_list [4], 3)

def test_gradient():
    '''This tests checks whether the rates of water level increase per day are computed correctly for stations 1 and 4.
    Also checks whether a TypeError is given for station 4 which has levels that are not floats'''
    gradient_of_level1 = [gradient(levels_list[0])[i] for i in range(5)]
    gradient_of_level2 = [gradient(levels_list[3])[i] for i in range(5)]
    assert set(gradient_of_level1) == {0, 0, 0, 0, 0}
    assert set(gradient_of_level2) == {96.0}
    with pytest.raises(TypeError):
        gradient(levels_list[4])

def average_gradient_n_hours_before_test():
    '''This tests whether a VelueError is given for the levels of station 2 which has too few levels. 
    Also checks whether a TypeError is given for station 4 which has levels that are not floats. 
    Checks whether average gradient is calculated correctly for stations 1 and 4'''
    with pytest.raises(ValueError):
        average_gradient_n_hours_before(levels_list[1], 1)
    with pytest.raises(TypeError):
        average_gradient_n_hours_before(levels_list[4], 1)
    assert (average_gradient_n_hours_before(levels_list[3], 1)) == 96.0
    assert (average_gradient_n_hours_before(levels_list[0], 1)) == 0.0
