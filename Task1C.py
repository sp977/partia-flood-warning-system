from floodsystem.geo import stations_by_distance
from floodsystem.geo import stations_within_radius
from floodsystem.station import MonitoringStation
from floodsystem.stationdata import build_station_list

def run():
    '''The function finds the stations within radius "r" from a location "centre" and outputs a list containing these stations sorted by distance'''
    stations = build_station_list()
    List_of_Stations_within_radius_full = stations_within_radius(stations, (52.2053, 0.1218), 10.0)
    print (len(List_of_Stations_within_radius_full))
    Updated_output_list = []
    for item in List_of_Stations_within_radius_full:
          station = item[0]
          Updated_output_list.append( station.name )    
    Updated_output_list.sort()
    print ( Updated_output_list)
    return None


if __name__ == "__main__":
    print("*** Task 1C: CUED Part IA Flood Warning System ***")
    run()