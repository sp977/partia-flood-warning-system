"""Unit test for plot module"""

import matplotlib.pyplot as plt
import datetime
import pytest
from floodsystem.flood import stations_highest_rel_level
from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.station import MonitoringStation
from floodsystem.datafetcher import fetch_measure_levels
from floodsystem.analysis import polyfit
from tests_water_stations_for_tests import s1, s2, s3, s4, s5, s_list, levels_list, dates_list
from floodsystem.plot import plot_water_levels, plot_water_level_with_fit


def plot_water_levels_test():
    '''The functions tests whether TypeError is given when the length of the dates list does not match length of levels list 
    for station 2 and whether a TypeError is given when the levels are not given as floats for station 5'''
    with pytest.raises(ValueError):
        plot_water_levels(s_list[1], dates_list[1], levels_list[1])
    with pytest.raises(ValueError):
        plot_water_levels(s_list[4], dates_list[4], levels_list[4])

def plot_water_level_with_fit_test():
    '''The functions tests whether TypeError is given when the length of the dates list does not match length of levels list 
    for station 2 and whether a TypeError is given when the levels are not given as floats for station 5'''
    with pytest.raises(TypeError):
        plot_water_level_with_fit(s_list[4], dates_list[4], levels_list[4], 3)
    with pytest.raises(TypeError):
        plot_water_level_with_fit(s_list[1], dates_list[1], levels_list[1], 3)

