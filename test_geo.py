"""Unit test for the geo module"""

from tests_water_stations_for_tests import s1, s2, s3, s4, s5, s_list
from floodsystem.station import MonitoringStation
from floodsystem.geo import stations_within_radius, stations_by_distance, stations_by_river, rivers_by_station_number, rivers_with_station
from haversine import haversine, Unit

def test_stations_by_distance():
    '''Tests whether the distance from the first station to (0,0) is correct'''
    output_list = stations_by_distance(s_list, (3,1.99))
    assert (output_list)[0][0] == s4
    assert (output_list)[4][0] == s5

def test_stations_within_radius_one(): 
    '''Tests whether there is a correct number of stations 0 km from (52.2053,0.1218)'''
    output_list = stations_within_radius(s_list, (3,1.99), 1.0)
    assert len(output_list) == 0

def test_rivers_with_station():
    '''Tests whether the list of rivers with stations is correct'''
    output_list = rivers_with_station(s_list)
    assert sorted(set(output_list)) == ['River H','River X', 'River Y', 'River Z']

def test_stations_by_river():
    '''Tests whether the correct stations on river X are shown'''
    output_dictionary = stations_by_river(s_list)
    assert output_dictionary['River Y'] == [s1, s3]

def test_rivers_by_station_number():
    '''Tests whether the names of rivers with greatest number of stations are shown correctly'''
    output_list = rivers_by_station_number(s_list,3)
    rivers_list = []
    for item in range(len(output_list)):
        rivers_list.append(output_list[item][0])
    assert rivers_list == ['River Y', 'River H', 'River X']
