import matplotlib.pyplot as plt
import datetime
from .flood import stations_highest_rel_level
from .stationdata import build_station_list, update_water_levels
from .station import MonitoringStation
from .datafetcher import fetch_measure_levels
from .analysis import polyfit

def plot_water_levels(station, dates, levels):

    # Plotting the values
    plt.plot(dates, levels, label = station.name)

    # Add axis labels, rotate date labels and add plot title
    plt.xlabel('date')
    plt.ylabel('relative water level')
    plt.xticks(rotation=45)
    plt.legend()

def plot_water_level_with_fit(station, dates, levels, p):
    poly, d0 = polyfit(dates, levels, p)
    timedeltasfloats = [[(date - d0).total_seconds()/(3600*24)] for date in dates]
   
    # Plot approximated data 
    plt.plot(dates, poly(timedeltasfloats), label = "approximated data for {}".format(station.name))

    # Plot original data points
    plot_water_levels(station, dates, levels)
  




