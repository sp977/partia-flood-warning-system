# Copyright (C) 2018 Garth N. Wells
#
# SPDX-License-Identifier: MIT
"""This module contains a collection of functions related to
geographical data.

"""
import operator
from .utils import sorted_by_key as sorted_by_key  # noqa 
from .station import MonitoringStation
from haversine import haversine, Unit

def stations_by_distance(stations, p):
    '''Given a list of 'station' objects and a set of coordinates 'p', will output (station,distance)
    tuples, sorted by distance '''
    #Errors implemented to make sure input is valid
    if p[0] > 90 or p[0] < -90:
        raise ValueError("Latitude must be between 90 and -90 degrees")
    if p[1] >180 or p[1] < -180:
        raise ValueError("Longitde must be between 180 and -180 degrees")
    Stations_and_Distances = []

    # Iteration through 'stations' list
    for station in stations:
        distance = haversine(station.coord , p)
        Stations_and_Distances.append((station, distance))

    # Sorting by distance    
    sorted_list = sorted_by_key (Stations_and_Distances,1)

    return sorted_list

def stations_within_radius(stations, centre, r):
    '''given the coordinates of a location "centre", this function will return the list of "stations" within radius "r" from "centre"'''
    if r < 0.0:
        raise ValueError("distance must be equal to or greater than 0 kilometres")
    elif type(stations[0]) != MonitoringStation:
        raise("The type entered is not Monitoring Weather Station")
    elif centre[0] > 90 or centre[0] < -90:
        raise ValueError("Latitude must be between 90 and -90 degrees")
    elif centre[1] >180 or centre[1] < -180:
        raise ValueError("Longitde must be between 180 and -180 degrees")
    Full_output_list = stations_by_distance(stations, centre)
    List_of_Stations_within_radius = []
    for item in Full_output_list:
        if item[1] > r:
            break
        else:
            List_of_Stations_within_radius.append((item))
    return List_of_Stations_within_radius



def rivers_with_station(stations):
    '''Makes a set of all the rivers given a list of MonitoringStation objects'''
    rivers = []
    for station in stations:
        rivers.append(station.river)
    return sorted(list(set(rivers)))

def stations_by_river(stations):
    '''Makes dictionary of rivers mapped to a list of all stations on that river given a list of MonitoringStation objects'''
    rivers = rivers_with_station(stations)
    rivers_dict = {}
    for river in rivers:
	    river_stations = []
	    for station in stations:
		    if station.river == river:
			    river_stations.append(station)
	    rivers_dict[river] = river_stations
    return rivers_dict

def rivers_by_station_number(stations, N):
    '''Given a list of MonitoringStation objects and number of entries, N, returns a list of (river name, number of stations) tuples, 
    sorted by the number of stations'''
    river_dict = stations_by_river(stations)
    out_put = []
    for key, value in river_dict.items():
        out_put.append((key, len(value)))
    out_put.sort(key = operator.itemgetter(1), reverse = True)
    return out_put[:N]
    



