import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import datetime

def polyfit(dates, levels, p):   
    #Converting dates to floats 
    if isinstance(dates[0], datetime.datetime) != True:
        raise TypeError("Date and time must be of Datetime class")
    elif type(levels[0]) != float and type(levels[0]) != int:
        raise TypeError("Levels must be floats")
    datesfloats = matplotlib.dates.date2num(dates)

    # Using shifted x values, find coefficient of best-fit
    p_coeff = np.polyfit(datesfloats - datesfloats[0], levels, p)

    # Convert coefficient into a polynomial that can be evaluated
    # e.g. poly(0.3)
    poly = np.poly1d(p_coeff)

    #Shift in time axis
    d0=dates[0]
    return poly, d0

def gradient(levels):
    '''This function takes in a list of levels and returns a 1D numpy array of numbers 
    indicating the rate of change of levels per day'''
    levelsnumpy = np.array(levels)
    grad = np.gradient(levelsnumpy, ((15/(60*24))))
    return grad

def average_gradient_n_hours_before(levels, n):
    '''This function takes in a list of dates, a list of levels and an integer n (which is the rounded up number of hours before now) 
    and returns the average rate of change of water levels per day'''
    grad = gradient(levels)
    if len(grad)< 3:
        raise ValueError("At least 3 values of gradient of levels must be inputted for an accurate calculation of mean")
    av_grad = np.mean(grad[((n*4)-4):(n*4)])
    return av_grad
