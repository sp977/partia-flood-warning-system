from floodsystem.station import MonitoringStation



def stations_level_over_threshold(stations, tol):
    '''Function that returns a list of tuples, where each tuple holds the station object and its latest water level, given a 
    list of station objects and a tolerance'''
    output_list = []
    for station in stations:
        if station.typical_range_consistent() == True:
            rwl = station.relative_water_level()
            if rwl != None:
                if rwl > tol:
                    output_list.append((station,rwl))
    output_list.sort(key = lambda x: x[1])
    output_list.reverse()
    return output_list

def stations_highest_rel_level(stations, N):
    ''' Function in the submodule flood that returns a list of the N stations (objects) at which the water level,
     relative to the typical range, is highest'''
    output_list = []
    for station in stations:
        if station.typical_range_consistent() == True:
            rwl = station.relative_water_level()
            if rwl != None:
                output_list.append([station,rwl])
    output_list.sort(key = lambda x: x[1])
    output_list.reverse()
    out_list = output_list[0:(N+1)]
    return out_list