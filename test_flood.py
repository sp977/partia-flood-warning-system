"""Unit test for the flood module"""

from floodsystem.flood import stations_level_over_threshold, stations_highest_rel_level
from tests_water_stations_for_tests import s1, s2, s3, s4, s5, s_list, dates_list, levels_list
import numpy as np
from floodsystem.station import MonitoringStation 
import pytest

def relative_water_level_test():
    '''This test should check if an Error is given for the relative water level of s5, which has the latest water level that is not 
    a float'''
    with pytest.raises(TypeError):
        s5.relative_water_level()

def stations_level_over_threshold_test():
    '''This function tests whether the correct number of water stations have relative water levels exceeding 1.5 which is 2'''
    tolerance = 1.5
    assert len(stations_level_over_threshold(s_list, tolerance)[0]) == 2 

def stations_highest_rel_level_test(): 
    '''This function tests whether the correct number of water stations are consistent and have a float for latest water level.
    It also tests that the first item in the list of highest relative water stations is station 2'''
    assert len(stations_highest_rel_level(s_list, 2)) == 3
    assert stations_highest_rel_level(s_list, 2)[0][0] == s2

