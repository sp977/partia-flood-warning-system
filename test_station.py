# Copyright (C) 2018 Garth N. Wells
#
# SPDX-License-Identifier: MIT
"""Unit test for the station module"""
from tests_water_stations_for_tests import s1, s2, s3, s4, s5, s_list
from floodsystem.station import inconsistent_typical_range_stations
from floodsystem.station import MonitoringStation
#from .floodsystem.station import 

'''Creating two test stations'''
#Test station 1


def test_create_monitoring_station():
    '''Tests whether the object attributes are correctly assigned'''
    s_idtest = "test-s-id"
    m_idtest = "test-m-id"
    labeltest = "some station"
    coordtest = (-2.0, 4.0)
    trangetest = (-2.3, 3.4445)
    rivertest = "River H"
    towntest = "My Town"
    station_test = MonitoringStation(s_idtest, m_idtest, labeltest, coordtest, trangetest, rivertest, towntest)
    assert station_test.station_id == s_idtest
    assert station_test.measure_id == m_idtest
    assert station_test.name == labeltest
    assert station_test.coord == coordtest
    assert station_test.typical_range == trangetest
    assert station_test.river == rivertest
    assert station_test.town == towntest

def test_typical_range_consistent():
    '''Tests whether the function works correctly for all test stations'''
    assert s1.typical_range_consistent() 
    assert s2.typical_range_consistent()
    assert s3.typical_range_consistent() == False
    assert s4.typical_range_consistent()
    assert s5.typical_range_consistent() == False
     

def test_inconsistent_typical_range_stations():
    '''Checks that the list of inconsistent stations is of the right length'''
    assert len(inconsistent_typical_range_stations(s_list)) == 2
