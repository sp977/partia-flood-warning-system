from floodsystem.station import MonitoringStation
from floodsystem.stationdata import build_station_list
from floodsystem.geo import rivers_with_station
from floodsystem.geo import stations_by_river
from floodsystem.geo import rivers_by_station_number

def run():

    # Building list of stations and set of rivers
    stations = build_station_list()

    # Getting output and and reversing tuples
    output = rivers_by_station_number(stations, 9)
    reversed_output = []
    for x in output:
        reversed_output.append(((x[1]),(x[0])))
    print (reversed_output)

if __name__ == "__main__":
    print("*** Task 1D: CUED Part IA Flood Warning System ***")
    run()
