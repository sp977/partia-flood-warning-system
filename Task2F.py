import matplotlib.pyplot as plt
import datetime
from floodsystem.flood import stations_highest_rel_level
from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.station import MonitoringStation
from floodsystem.datafetcher import fetch_measure_levels
from floodsystem.plot import plot_water_levels, plot_water_level_with_fit
from floodsystem.analysis import polyfit

# Creating a list of 5 stations which had the highest water level, trimmining the list if there are more than 5 stations
def run():
    dt = 2
    stations = build_station_list()
    update_water_levels(stations)
    output_list = stations_highest_rel_level(stations,5)
    trimmed_output_list = output_list[:5]

    #Iterating over the output list to produce the graph of relative water level against date for each station"
    for i in range(len(trimmed_output_list)):
        #Setting up the dates and levels
        datescurrent, levelshighest = fetch_measure_levels(trimmed_output_list[i][0].measure_id, datetime.timedelta(days=dt))
        #Plotting the water levels with fit using the trimed output list
        plot_water_level_with_fit(trimmed_output_list[i][0], datescurrent, levelshighest, 4)
        #Setting up the values for the typical range
        typical_range_min_value_list = [trimmed_output_list[i][0].typical_range[0] for date in datescurrent]
        typical_range_max_value_list = [trimmed_output_list[i][0].typical_range[1] for date in datescurrent]
        #Plotting the typical range
        plt.plot(datescurrent, typical_range_min_value_list, label = "typical low")
        plt.plot(datescurrent, typical_range_max_value_list, label = "typical high")
        #Adding the legend, title and layout to each plot
        plt.title(trimmed_output_list[i][0].name)
        plt.tight_layout()
        plt.legend()
        plt.show()

if __name__ == "__main__":
    print("*** Task 2F: CUED Part IA Flood Warning System ***")
    run()
    

