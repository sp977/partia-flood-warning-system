from floodsystem.flood import stations_highest_rel_level
from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.station import MonitoringStation
import matplotlib.pyplot as plt
import datetime
from floodsystem.datafetcher import fetch_measure_levels
from floodsystem.plot import plot_water_levels

def run():
    start_time_of_function = datetime.datetime.now()
    #dt - number of days
    dt = 5 
    # Creating a list of 5 stations which had the highest water level, trimmining the list if there are more than 5 stations
    stations = build_station_list()
    update_water_levels(stations)
    output_list = stations_highest_rel_level(stations,5)
    trimmed_output_list = output_list[:5]

    #Iterating over the output list to produce the graph of relative water level against date for each station"
    for i in range(len(trimmed_output_list)):
        datescurrent, levelshighest = fetch_measure_levels(
            trimmed_output_list[i][0].measure_id, datetime.timedelta(days=dt))
        plot_water_levels(trimmed_output_list[i][0], datescurrent, levelshighest)
    plt.title("Stations with highest relative water levels")
    plt.tight_layout()
    plt.legend()
    plt.show()
    print("It took {} to execute the function".format(datetime.datetime.now()- start_time_of_function))

if __name__ == "__main__":
    print("*** Task 2E: CUED Part IA Flood Warning System ***")
    run()
    