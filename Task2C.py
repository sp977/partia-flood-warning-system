from floodsystem.flood import stations_level_over_threshold, stations_highest_rel_level
from floodsystem.station import MonitoringStation
from floodsystem.stationdata import build_station_list, update_water_levels

def run():

    # Building list of stations 
    stations = build_station_list()

    # Update latest level data for all stations
    update_water_levels(stations)

    # Printing the name of each station at which the current relative level is over 0.8, with the relative level alongside the name
    output_list = stations_highest_rel_level(stations,10)
    for item in output_list:
        print(item[0].name, ' ' , item[1], '\n' )

if __name__ == "__main__":
    print("*** Task 2C: CUED Part IA Flood Warning System ***")
    run() 