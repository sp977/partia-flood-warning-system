from floodsystem.station import MonitoringStation
from floodsystem.stationdata import build_station_list
from floodsystem.geo import rivers_with_station
from floodsystem.geo import stations_by_river
from floodsystem.geo import rivers_by_station_number

def run():

    # Building list of stations and set of rivers
    stations = build_station_list()
    rivers = rivers_with_station(stations)

    # Print number of rivers
    print("Number of rivers: {}".format(len(rivers)))
	
    # Print first 10 rivers 
    rivers.sort()
    print(rivers[:10])

    # Building dictionary of rivers
    rivers_dict = stations_by_river(stations)

    # Printing stations on River Aire
    stations_Aire = rivers_dict['River Aire']
    station_names = []
    for station in stations_Aire:
        station_names.append(station.name)
    station_names.sort()
    print (station_names)

    # Printing stations on River Cam
    stations_Cam = rivers_dict['River Cam']
    station_names = []
    for station in stations_Cam:
        station_names.append(station.name)
    station_names.sort()
    print (station_names)

    # Printing stations on River Thames
    stations_Thames = rivers_dict['River Severn']
    station_names = []
    for station in stations_Thames:
        station_names.append(station.name)
    station_names.sort()
    print (station_names)

if __name__ == "__main__":
    print("*** Task 1D: CUED Part IA Flood Warning System ***")
    run()