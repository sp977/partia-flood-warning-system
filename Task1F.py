from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.station import MonitoringStation, inconsistent_typical_range_stations

def run():
    '''the function returns an alphabetically sorted list of stations with inconsistent typical minimum and maximum water levels '''
    stations = build_station_list()
    Updated_output_list = []
    for station in inconsistent_typical_range_stations(stations):
          Updated_output_list.append( station.name )    
    Updated_output_list.sort()
    print ( Updated_output_list)
    
if __name__ == "__main__":
    print("*** Task 1F: CUED Part IA Flood Warning System ***")
    run()
    
     