# Copyright (C) 2018 Garth N. Wells
#
# SPDX-License-Identifier: MIT
"""Unit test for the datafetcher module"""

import datetime

from floodsystem.datafetcher import fetch_measure_levels, fetch_measure_levels_no_key_error
from floodsystem.stationdata import build_station_list


def test_build_station_list():

    # Build list of stations
    stations = build_station_list()

    # Find station 'Cam'
    for station in stations:
        if station.name == 'Cam':
            station_cam = station
            break

    # Assert that station is found
    assert station_cam

    # Fetch data over past 2 days
    dt = 2
    dates2, levels2 = fetch_measure_levels(
        station_cam.measure_id, dt=datetime.timedelta(days=dt))
    assert len(dates2) == len(levels2)

    # Fetch data over past 10 days
    dt = 10
    dates10, levels10 = fetch_measure_levels(
        station_cam.measure_id, dt=datetime.timedelta(days=dt))
    assert len(dates10) == len(levels10)
    assert len(dates10) > len(levels2)

def test_fetch_water_levels_no_key_error():
    '''This test finds a station which has no water levels recorded and checks that all water levels are of None type for that station'''
    dt = 1
    stations = build_station_list()
    for station in stations:
        levels = fetch_measure_levels_no_key_error(station.measure_id, datetime.timedelta(days=dt))[1]
        levels_no_Nones = [level for level in levels if isinstance(level, float)]
        if len(levels_no_Nones) == 0:
            station_no_lvl = station
            break
        else:
            pass

    levels_station = fetch_measure_levels_no_key_error(station_no_lvl.measure_id, datetime.timedelta(days=dt))[1]
    assert(all(level is None for level in levels_station)) 






    
