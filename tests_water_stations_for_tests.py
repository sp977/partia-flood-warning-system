"""A file containing data about 5 statitions the unit tests are using to check the validity of function outputs"""

from floodsystem.station import MonitoringStation
import datetime 


'''Creating 4 test stations'''
#Test station 1
s_id = "test-s-id"
m_id = "test-m-id"
label = "some station"
coord = (2.5,0)
trange = (-2.3, 3.4445)
river = "River Y"
town = "My Town"
latestlevel1 = 1.2
dictionary_for_dates_and_levels = {}

#Test station 2
s_id2 = "test-s-id2"
m_id2= "test-m-id2"
label2 = "some station2"
coord2 = (0,1)
trange2 = (-2.5, 3.6)
typicalrange = 3.2
latestlevel2 = 8.7
river2 = "River X"
town2 = "My Town2"

#Test station 3
s_id3 = "test-s-id3"
m_id3= "test-m-id3"
label3 = "some station3"
coord3 = (2,1)
trange3 = (2.5)
latestlevel3 = 8.6
river = "River Y"
town3 = "My Town2"

#Test station 4
s_id4 = "test-s-id4"
m_id4= "test-m-id4"
label4 = "some station4"
coord4 = (3,1)
trange4 = (2.5,3.5)
latestlevel4 = 1.5
river4 = "River Z"
town4 = "My Town2"


#Test station 5
s_id5 = "test-s-id5"
m_id5= "test-m-id5"
label5 = "some station5"
coord5 = (3,10)
trange5 = (2.5)
latestlevel5 = 'a'
river5 = "River H"
town5 = "My Town2"

#Assigning levels to the stations
timedel = datetime.timedelta(minutes = 15)
timenow = datetime.datetime.now()
datess1to4 = [(timenow - timedel * i) for i in range(5)]
datess5 = [0,15,30,45,60]
levels1 = [1,1,1,1,1]
levels2 = [2,2]
levels3 = [1,4,9,16,25]
levels4 = [0.0, 1.0, 2.0 , 3.0 , 4.0]
levels5 = ['g', 'g', 'i', 's', 's']
dates_list = [datess1to4, datess1to4, datess1to4, datess1to4, datess5]
levels_list = [levels1, levels2, levels3, levels4, levels5]

s1 = MonitoringStation(s_id, m_id, label, coord, trange, river, town)
s1.latest_level = latestlevel1
s2 = MonitoringStation(s_id2, m_id2, label2, coord2, trange2, river2, town2)
s2.latest_level = latestlevel2
s3 = MonitoringStation(s_id3, m_id3, label3, coord3, trange3, river, town3)
s3.latest_level = latestlevel3
s4 = MonitoringStation(s_id4, m_id4, label4, coord4, trange4, river4, town4)
s4.latest_level = latestlevel4
s5 = MonitoringStation(s_id5, m_id5, label5, coord5, trange5, river5, town5,)
s5.latest_level = latestlevel5
s_list = [s1, s2, s3, s4, s5]

