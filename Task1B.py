from floodsystem.geo import stations_by_distance
from floodsystem.station import MonitoringStation
from floodsystem.stationdata import build_station_list

def run():
    """Requirements for Task 1B"""

    # Build list of stations
    stations = build_station_list()

    # Build full list
    Full_output_list = stations_by_distance(stations, (52.2053, 0.1218))

    Updated_output_list = []

    # Iterating through stations
    for item in Full_output_list:
        station = item[0]
        Updated_output_list.append( (station.name , station.town , item[1]) )
    
    # Returning a list of closest and furthest stations
    print(Updated_output_list[:10] + Updated_output_list[-10:])

if __name__ == "__main__":
    print("*** Task 1B: CUED Part IA Flood Warning System ***")
    run()


    


